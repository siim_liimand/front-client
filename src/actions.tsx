import { AppProps } from '@app/interfaces';
import { isImported } from '@dixid/dynamic-import';
import { ReactFCE, unmountComponentAtNode } from '@lib/xreact';
import { hydrate } from 'preact';
import { App } from '@app/index';

const loaderElId = 'r';

export function waitAndAskIfImportsAreLoaded(): void {
  setTimeout(() => {
    if (isImported()) {
      replaceRootElement();
    } else {
      waitAndAskIfImportsAreLoaded();
    }
  }, 50);
}

/**
 * Replace root element
 */
function replaceRootElement() {
  removeLoaderContainer();
  const rootElement: Element = document.getElementById('root') || document.createElement('<div>');
  hydrate(createAppElement(), rootElement);
}

/**
 * Create App element
 * @returns {ReactFCE<AppProps>}
 */
export function createAppElement(): ReactFCE<AppProps> {
  const location = window.location;
  const hostName = location.hostname;
  const path = `${location.pathname}`;
  const query = new URLSearchParams(location.search);

  return <App h={hostName} p={path} q={query} />;
}

/**
 * Create loader container
 * @returns {Element}
 */
export function createLoaderContainer(): Element {
  const loader = document.createElement('div');
  loader.style.display = 'none';
  loader.id = loaderElId;
  document.body.appendChild(loader);

  return loader;
}

/**
 * Remmove loader container
 */
function removeLoaderContainer(): void {
  const loaderElement = document.getElementById(loaderElId);
  if (loaderElement) {
    unmountComponentAtNode(loaderElement);
    loaderElement.innerHTML = '';
    loaderElement.remove();
  }
}
