export type Hostname = string;
export type Paths = string;
export type Query = URLSearchParams;
export interface AppProps {
  h: Hostname;
  p: Paths;
  q: Query;
}
