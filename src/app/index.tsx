import { dynamicImport, GetComponentCallback } from '@dixid/dynamic-import';
import { Fragment, ReactFC, ReactFCE } from '@lib/xreact';
import { getRoute } from '@dixid/front-router';

import { AppProps, Paths } from './interfaces';
import { GetPageComponent, GetPageComponentCallback, PageProps } from '@dixid/front-router/dist/interfaces';
import { getPageData } from '@dixid/front-services';

export const App: ReactFC<AppProps> = (props): ReactFCE<PageProps> => {
  const p: Paths = props.p;

  const getComponentCallback: GetComponentCallback = (resolve): void => {
    const route = getRoute(p);
    if (route) {
      const pageComponentCallback = route[1] as GetPageComponentCallback;
      const getPageComponent: GetPageComponent = (Component) => {
        const siteName = '';
        const query = new URLSearchParams();
        getPageData(
          fetch,
          siteName,
          p,
          query,
          (pageData) => {
            resolve(<Component pageData={pageData} />);
          },
          (error: Error | string) => {
            resolve(<ErrorComponent errorMessage={`${error}`} />);
          },
        );
      };
      pageComponentCallback(getPageComponent);
    } else {
      resolve(<ErrorComponent errorMessage="2" />);
    }
  };

  const Component = dynamicImport('p', getComponentCallback);

  return <Component />;
};

interface ErrorComponentProps {
  errorMessage: string;
}

function ErrorComponent(props: ErrorComponentProps): ReactFCE<unknown> {
  return <Fragment>{props.errorMessage}</Fragment>;
}
