import { render } from 'preact';
// import { addRoute } from '@dixid/front-router';
import '@dixid/dynamic-import';
import { createAppElement, createLoaderContainer, waitAndAskIfImportsAreLoaded } from './actions';

// addRoute('/', (getPageComponent): void => {
//   import(/* webpackChunkName: "home" */ '@pages/home').then((module) => {
//     getPageComponent(module.default);
//   });
// });

// createClient();

/**
 * Create client
 */
export function createClient(): void {
  render(createAppElement(), createLoaderContainer());
  waitAndAskIfImportsAreLoaded();
}
