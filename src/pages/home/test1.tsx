import { useTestStore, TestStore } from '@dixid/test-store';

export const Test1 = () => {
  const store: TestStore = useTestStore();
  const onTestAction = store[3];

  const onClickHandler = (e: any) => {
    e.preventDefault();
    onTestAction();
  };

  return (
    <div>
      <a href="#" onClick={onClickHandler}>
        Test1
      </a>
    </div>
  );
};
