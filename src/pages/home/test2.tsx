import { useEffect, useState } from '@lib/xreact';
import { useTestStore, IsNavigationOpen, TestStore } from '@dixid/test-store';
import { Subscribe } from '@dixid/tiny-redux';

export const Test2 = () => {
  const store: TestStore = useTestStore();
  const subscibe: Subscribe = store[2];
  const isNavigationOpen: IsNavigationOpen = store[4];
  const [showNavigation, setShowNavigation] = useState<boolean>(isNavigationOpen());

  const navigationStatusListener = (): void => {
    setShowNavigation(isNavigationOpen());
  };

  useEffect(() => {
    const unsubscribe = subscibe(navigationStatusListener);

    return () => {
      unsubscribe();
    };
  }, []);

  return <div>{showNavigation && <div>Navigation</div>}</div>;
};
