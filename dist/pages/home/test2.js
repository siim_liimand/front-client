"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Test2 = void 0;
const jsx_runtime_1 = require("preact/jsx-runtime");
const xreact_1 = require("../../lib/xreact");
const test_store_1 = require("@dixid/test-store");
const Test2 = () => {
    const store = (0, test_store_1.useTestStore)();
    const subscibe = store[2];
    const isNavigationOpen = store[4];
    const [showNavigation, setShowNavigation] = (0, xreact_1.useState)(isNavigationOpen());
    const navigationStatusListener = () => {
        setShowNavigation(isNavigationOpen());
    };
    (0, xreact_1.useEffect)(() => {
        const unsubscribe = subscibe(navigationStatusListener);
        return () => {
            unsubscribe();
        };
    }, []);
    return (0, jsx_runtime_1.jsx)("div", { children: showNavigation && (0, jsx_runtime_1.jsx)("div", { children: "Navigation" }, void 0) }, void 0);
};
exports.Test2 = Test2;
//# sourceMappingURL=test2.js.map