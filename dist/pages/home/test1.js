"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Test1 = void 0;
const jsx_runtime_1 = require("preact/jsx-runtime");
const test_store_1 = require("@dixid/test-store");
const Test1 = () => {
    const store = (0, test_store_1.useTestStore)();
    const onTestAction = store[3];
    const onClickHandler = (e) => {
        e.preventDefault();
        onTestAction();
    };
    return ((0, jsx_runtime_1.jsx)("div", { children: (0, jsx_runtime_1.jsx)("a", Object.assign({ href: "#", onClick: onClickHandler }, { children: "Test1" }), void 0) }, void 0));
};
exports.Test1 = Test1;
//# sourceMappingURL=test1.js.map