"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jsx_runtime_1 = require("preact/jsx-runtime");
const test1_1 = require("./home/test1");
const test2_1 = require("./home/test2");
const Home = () => {
    return ((0, jsx_runtime_1.jsxs)("div", { children: [(0, jsx_runtime_1.jsx)(test1_1.Test1, {}, void 0), (0, jsx_runtime_1.jsx)("br", {}, void 0), (0, jsx_runtime_1.jsx)(test2_1.Test2, {}, void 0)] }, void 0));
};
exports.default = Home;
//# sourceMappingURL=home.js.map