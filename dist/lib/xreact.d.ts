import { Context, Fragment, FunctionComponent, h, render, VNode as FunctionComponentElement } from 'preact';
import 'preact/debug';
export declare type ReactFCE<P> = FunctionComponentElement<P>;
export declare type ReactFC<P> = FunctionComponent<P>;
export { h, render, Fragment };
export { hydrate, createContext, createElement } from 'preact';
export type { Context };
export { useState, useEffect, useContext } from 'preact/hooks';
/**
 * Unmount component at node
 * @param {Element} element
 */
export declare function unmountComponentAtNode(element: Element): void;
//# sourceMappingURL=xreact.d.ts.map