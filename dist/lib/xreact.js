"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.unmountComponentAtNode = exports.useContext = exports.useEffect = exports.useState = exports.createElement = exports.createContext = exports.hydrate = exports.Fragment = exports.render = exports.h = void 0;
const preact_1 = require("preact");
Object.defineProperty(exports, "Fragment", { enumerable: true, get: function () { return preact_1.Fragment; } });
Object.defineProperty(exports, "h", { enumerable: true, get: function () { return preact_1.h; } });
Object.defineProperty(exports, "render", { enumerable: true, get: function () { return preact_1.render; } });
require("preact/debug");
var preact_2 = require("preact");
Object.defineProperty(exports, "hydrate", { enumerable: true, get: function () { return preact_2.hydrate; } });
Object.defineProperty(exports, "createContext", { enumerable: true, get: function () { return preact_2.createContext; } });
Object.defineProperty(exports, "createElement", { enumerable: true, get: function () { return preact_2.createElement; } });
var hooks_1 = require("preact/hooks");
Object.defineProperty(exports, "useState", { enumerable: true, get: function () { return hooks_1.useState; } });
Object.defineProperty(exports, "useEffect", { enumerable: true, get: function () { return hooks_1.useEffect; } });
Object.defineProperty(exports, "useContext", { enumerable: true, get: function () { return hooks_1.useContext; } });
/**
 * Unmount component at node
 * @param {Element} element
 */
function unmountComponentAtNode(element) {
    (0, preact_1.render)((0, preact_1.h)(preact_1.Fragment, null), element);
}
exports.unmountComponentAtNode = unmountComponentAtNode;
//# sourceMappingURL=xreact.js.map