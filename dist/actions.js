"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createLoaderContainer = exports.createAppElement = exports.waitAndAskIfImportsAreLoaded = void 0;
const jsx_runtime_1 = require("preact/jsx-runtime");
const dynamic_import_1 = require("@dixid/dynamic-import");
const xreact_1 = require("./lib/xreact");
const preact_1 = require("preact");
const index_1 = require("./app/index");
const loaderElId = 'r';
function waitAndAskIfImportsAreLoaded() {
    setTimeout(() => {
        if ((0, dynamic_import_1.isImported)()) {
            replaceRootElement();
        }
        else {
            waitAndAskIfImportsAreLoaded();
        }
    }, 50);
}
exports.waitAndAskIfImportsAreLoaded = waitAndAskIfImportsAreLoaded;
/**
 * Replace root element
 */
function replaceRootElement() {
    removeLoaderContainer();
    const rootElement = document.getElementById('root') || document.createElement('<div>');
    (0, preact_1.hydrate)(createAppElement(), rootElement);
}
/**
 * Create App element
 * @returns {ReactFCE<AppProps>}
 */
function createAppElement() {
    const location = window.location;
    const hostName = location.hostname;
    const path = `${location.pathname}`;
    const query = new URLSearchParams(location.search);
    return (0, jsx_runtime_1.jsx)(index_1.App, { h: hostName, p: path, q: query }, void 0);
}
exports.createAppElement = createAppElement;
/**
 * Create loader container
 * @returns {Element}
 */
function createLoaderContainer() {
    const loader = document.createElement('div');
    loader.style.display = 'none';
    loader.id = loaderElId;
    document.body.appendChild(loader);
    return loader;
}
exports.createLoaderContainer = createLoaderContainer;
/**
 * Remmove loader container
 */
function removeLoaderContainer() {
    const loaderElement = document.getElementById(loaderElId);
    if (loaderElement) {
        (0, xreact_1.unmountComponentAtNode)(loaderElement);
        loaderElement.innerHTML = '';
        loaderElement.remove();
    }
}
//# sourceMappingURL=actions.js.map