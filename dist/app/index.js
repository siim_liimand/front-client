"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const jsx_runtime_1 = require("preact/jsx-runtime");
const dynamic_import_1 = require("@dixid/dynamic-import");
const xreact_1 = require("../lib/xreact");
const front_router_1 = require("@dixid/front-router");
const front_services_1 = require("@dixid/front-services");
const App = (props) => {
    const p = props.p;
    const getComponentCallback = (resolve) => {
        const route = (0, front_router_1.getRoute)(p);
        if (route) {
            const pageComponentCallback = route[1];
            const getPageComponent = (Component) => {
                const siteName = '';
                const query = new URLSearchParams();
                (0, front_services_1.getPageData)(fetch, siteName, p, query, (pageData) => {
                    resolve((0, jsx_runtime_1.jsx)(Component, { pageData: pageData }, void 0));
                }, (error) => {
                    resolve((0, jsx_runtime_1.jsx)(ErrorComponent, { errorMessage: `${error}` }, void 0));
                });
            };
            pageComponentCallback(getPageComponent);
        }
        else {
            resolve((0, jsx_runtime_1.jsx)(ErrorComponent, { errorMessage: "2" }, void 0));
        }
    };
    const Component = (0, dynamic_import_1.dynamicImport)('p', getComponentCallback);
    return (0, jsx_runtime_1.jsx)(Component, {}, void 0);
};
exports.App = App;
function ErrorComponent(props) {
    return (0, jsx_runtime_1.jsx)(xreact_1.Fragment, { children: props.errorMessage }, void 0);
}
//# sourceMappingURL=index.js.map