export declare type Hostname = string;
export declare type Paths = string;
export declare type Query = URLSearchParams;
export interface AppProps {
    h: Hostname;
    p: Paths;
    q: Query;
}
//# sourceMappingURL=interfaces.d.ts.map