import { AppProps } from './app/interfaces';
import { ReactFCE } from './lib/xreact';
export declare function waitAndAskIfImportsAreLoaded(): void;
/**
 * Create App element
 * @returns {ReactFCE<AppProps>}
 */
export declare function createAppElement(): ReactFCE<AppProps>;
/**
 * Create loader container
 * @returns {Element}
 */
export declare function createLoaderContainer(): Element;
//# sourceMappingURL=actions.d.ts.map