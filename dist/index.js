"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createClient = void 0;
const preact_1 = require("preact");
// import { addRoute } from '@dixid/front-router';
require("@dixid/dynamic-import");
const actions_1 = require("./actions");
// addRoute('/', (getPageComponent): void => {
//   import(/* webpackChunkName: "home" */ '@pages/home').then((module) => {
//     getPageComponent(module.default);
//   });
// });
// createClient();
/**
 * Create client
 */
function createClient() {
    (0, preact_1.render)((0, actions_1.createAppElement)(), (0, actions_1.createLoaderContainer)());
    (0, actions_1.waitAndAskIfImportsAreLoaded)();
}
exports.createClient = createClient;
//# sourceMappingURL=index.js.map